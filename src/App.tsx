import React, { Component } from 'react';
import Feedback from './components/Feedback/Feedback';
import s from './box.module.scss';

class App extends Component {
  render() {
    return (
      <div className="app">
        <section className={s.box}>
          <Feedback />
        </section>
      </div>
    );
  }
}

export default App;
