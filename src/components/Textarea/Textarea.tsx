import React from 'react';
import s from './Textarea.module.scss';

type Props = {
  placeholder?: string,
  name: string,
  disabled?: boolean,
  onChange?: (e: React.FormEvent<HTMLTextAreaElement>) => void;
  required?: boolean,
  value: string
}

const Textarea: React.FC<Props> = (props) => {
  return (
    <div className={s.wrap}>
      <textarea className={s.input} {...props}></textarea>
    </div>
  )
}

export default Textarea;