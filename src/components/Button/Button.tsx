import React from 'react';
import s from './Button.module.scss'

type Props = {
  children?: any,
  type?: string,
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void,
  disabled?: boolean,
  name?: string,
  className?: string
}

const Button:React.FC<Props> = ({children, ...props}) => {
  return (
    <button className={s.btn} {...props}>{children}</button>
  )
}

export default Button;