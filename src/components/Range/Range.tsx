import React from 'react';
import s from './Range.module.scss';

type Props = {
  
};

const Range: React.FC<Props> = props => {
  return (
    <div className={s.wrap}>
      <div className={s.group}>
        <input type="range" className={s.slider} id="start" name="start" min="12" max="16"/>
        <input type="range" className={s.slider} id="end" name="end" min="17" max="21"/>
      </div>
      <span className={s.hours}>
        <span>12</span>
        <span>13</span>
        <span>14</span>
        <span>15</span>
        <span>16</span>
        <span>17</span>
        <span>18</span>
        <span>19</span>
        <span>20</span>
        <span>21</span>
      </span>
    </div>
  );
};

export default Range;