import React from 'react';
import s from './Checkbox.module.scss';

type Props = {
  children: any,
  checked?: boolean,
  name?: string,
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void,
  disabled?: boolean
}

const Checkbox:React.FC<Props> = ({children, ...props}) => {
  return (
    <label className={s.checkbox}>
      <input type="checkbox" className={s.invisible} {...props}/>
      <span className={s.square}></span>
      <span className={s.text}>{children}</span>
    </label>
  )
}

export default Checkbox;