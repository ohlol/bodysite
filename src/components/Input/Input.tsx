import React from 'react';
import s from './Input.module.scss';

type Props = {
  placeholder?: string;
  type: string;
  name: string;
  disabled?: boolean;
  onChange?: (e: React.FormEvent<HTMLInputElement>) => void;
  required?: boolean;
  id?: string;
  children: string;
  value: string;
};

const Input: React.FC<Props> = ({ id, children, required, ...props }) => {
  return (
    <div className={s.input}>
      <label className={s.label} htmlFor={id}>
        {children} {' '}
        {required && <span className={s.required}>*</span>}
      </label>
      <input id={id} className={s.field} {...props} />
    </div>
  );
};

export default Input;
