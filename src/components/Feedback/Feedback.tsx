import React, { Component } from 'react';

import { ReactComponent as Img } from "./close.svg";
import s from './Feedback.module.scss';
import Input from '../Input/Input';
import Textarea from '../Textarea/Textarea';
import Button from '../Button/Button';
import Checkbox from '../Checkbox/Checkbox';
import Range from '../Range/Range';


interface IFormFields {
  name: string,
  phone: string,
  comment: string,
}

interface IState {
  data: IFormFields,
  checkbox: boolean
}

class Feedback extends Component<{}, IState> {
  public state = {
    data: {
      name: '',
      phone: '',
      comment: '',
      
    },
    checkbox: false,
    required: true
  }
  public render() {
    const { data, checkbox, required } = this.state
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <div className={s.header}>
          <h2 className={s.heading}>Заказать обратный звонок</h2>
          <Button className={s.close}><Img /></Button>
          <span className={s.note}>
            Оставьте свои контакты и мы перезвоним Вам
          </span>
        </div>
        <div className={s.field}>
          <h3 className={s.subheading}>Контактная информация</h3>
          <Input 
            name="name" 
            type="text" 
            id="name" 
            value={data.name}
            onChange={this.handleInput.bind(this)}
            required={required}
          >
            Имя
          </Input>
          <Input 
            name="phone" 
            type="tel" 
            id="phone" 
            value={data.phone}
            onChange={this.handleInput.bind(this)}
            required={required}
          >
            Телефон
          </Input>
        </div>
        <div className={s.field}>
          <h3 className={s.subheading}>Время для звонка</h3>
          <Range />
        </div>
        <div className={s.field}>
          <h3 className={s.subheading}>Комментарий</h3>
          <Textarea 
            name="comment"
            value={data.comment}
            onChange={this.handleInput.bind(this)}
          />
        </div>
        <Checkbox 
          checked={checkbox}
          onChange={this.handleCheckbox.bind(this)}
        >
          <span>Согласен на обработку <a href="/" className={s.link}>персональных данных</a></span>
        </Checkbox>
        <Button>
          Заказать
        </Button>
      </form>
    )
  }

  private handleSubmit(event: React.FormEvent) {
    event.preventDefault();
  }

  private handleInput({target}: React.FormEvent) {
    const {name, value} = target as HTMLInputElement;
    const data: any = this.state.data;
    data[name] = value;
    this.setState(state => ({
      ...state,
      data
    }))
  }

  private handleCheckbox() {
    this.setState(({checkbox}) => ({
      checkbox: !checkbox
    }))
  }

}

export default Feedback;